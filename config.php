<?php

/*
 * Here you can set all needed settings
 */

return array(
		// Name of logfile
		// For security reason use a speacial filename ex: MyL0gF1l3.log (Please don't use this one)
		// Or place this file outside www-root folder, but reachable for the webserver.
		logfile => "logfile.log",
		
		// Enable or disable logging to logfile. If false the logfile will be deleted. Default = false
		logging => true,
		
		// Name of database file, please place the file outside the www-root folder but reachable for the webserver.
		// Use a long filename ex: nzzXTzSbUezCXkE5dTE2.sqlite
		dbFile => "TugRDgNy6CX5EeB.sqlite",
		
		// Array with Device IDs. 
		// Ids seperated by comma ("ID1", "ID2", "ID3")
		deviceIds => array("YOUR_IDs"),
		
		// Set the link to your homematic ccu1 or ccu2. Example: http://homematic-ccu2 
		// or user the IP Address http://192.168.0.22 (without slash at the end)
		ccuUrl => "http://homematic-ccu2",
		
		// Set the ise_id of your systemvariable or programm. Example ise_id Value 950
		ise_id => "950",
		
		// Show Device ID (If you want to display the ID)
		// If true only the id is shown, the script will not be executed.
		// Default = false.
		showDeviceId => false,
		
		// For Debugging enable this. The log message will be shown on device screen 
		// after you hit the button "Verbindungstest", default = false.
		showLog => false,
		
		// If you want to use Push-Notification by using Prowl or Pushover, change this Values to true.
		// geoProwl.php or geoPushover.php or both are required.
		useProwl => false,
		usePushover => false,
		
		// If you want to use own actions on your CCU by device fill out this array.
		// For more information take a look at: http://www.homematic-inside.de/software/addons/item/xmlapi
		// You can use "runprogram.cgi" or "statechange.cgi".
		
		// Actions at arrive
		ownActionsArrive => array(
			'DEVICE_ID1' => 'statechange.cgi?ise_id=1452&new_value=1',
			'DEVICE_ID2' => 'runprogram.cgi?program_id=1234'
			),
		
		// Actions at leave
		ownActionsLeave => array(
			'DEVICE_ID1' => 'statechange.cgi?ise_id=1452&new_value=0',
			'DEVICE_ID2' => 'runprogram.cgi?program_id=1234'
			),
                
                );