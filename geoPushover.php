<?php

	/*
	Get data from geo.php (Geofency2Homematic) and send a Push-Notification to your device with Pushover.
	Copyright (C) 2014  Philipp Heumos
	Version 0.1
	
	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.
	
	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.
	
	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
	*/
	
	/*
	IMPORTEND:
	If you want to use this, you need a Pushover account, for more Information take a look at Pushover webseite:
	https://pushover.net
	*/
	
	/* NOTE: This file is untested. */

 class GeoPushover {
 	
 	// Enter your Pushover API / Token Key for the Application
 	protected $token = "FILL_OUT";
 	
 	// Enter your Pushover User Key (this one on the Dashboard)
 	protected $user = "FILL_OUT";
 	
 	// Enter your message is shown at entry.
 	protected $presentMessage = "Anwesend";
 	
 	// // Enter your message is shown at leaving.
 	protected $awayMessage = "Abwesend";
 	
 	// Enter your errormessage
 	protected $errMessage = "Es ist ein Fehler beim setzen des Status auf der CCU aufgetreten";
 	
 	
 	/********************************************************************
 	* 				NOTHING TO CHANGE PAST THIS LINE					*
	********************************************************************/
	 	
	protected $locationName = NULL;
 	protected $entry = NULL;
 	 	
 	public function __construct($entry, $locationName) {
 		
 		$this->locationName = $locationName;
 		$this->entry = $entry;
	 	$this->genText();
 	}
 	
 	protected function genText() {
 		switch ($this->entry) {
 		
	 		case 0:
	 			$this->event = $this->locationName . ": " . $this->awayMessage;
	 			break;
	 			
	 		case 1:
	 			$this->event = $this->locationName . ": " . $this->presentMessage;
	 			break;
	 			
	 		case 2:
	 			$this->event = "Error: " . $this->locationName . ": " . $this->errMessage;
	 			break;
	 	}
 	
	 	$this->sendToPushover();
 	}

	protected function sendToPushover() {
	  
		# Our new data
		$data = array('token' => $this->token, 'user' => $this->user, 'title' => "Geofency2Homematic", 'message'=> $this->event);
		
		# Create a connection
		$url = 'https://api.pushover.net/1/messages.json';
		$ch = curl_init($url);
		
		# Form data string
		$postString = http_build_query($data, '', '&');
				
		# Setting our options
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				
		# Get the response
		curl_exec($ch);
		curl_close($ch);
	 }
 }
?>