	<?php
       /*
	* What you need:
	* 1. Homematic CCU1 or CCU2
	* 2. Installed XML RPC Patch on your CCU (http://www.homematic-inside.de/software/addons/item/xmlapi)
	* 3. Working Webserver with PHP reachable from www and in the same network as your CCU.
	* 4. Geofency App from AppStore
	*/
        
        /*
         * 
         * 
         * CONFIGURATION MOVED TO "config.php"
         * 
         * 
         * 
         * Prowl config in "geoProwl.php"
         * Pushover config in "geoPushover.php"
         */
        
        /****************************************************
        *
        * Nothing to change past this comment
        *
        *
        */
     
	class GeoClass {
		
		protected $deviceId = NULL;
		protected $entry = NULL;
		protected $isEntry = NULL;
		protected $locationName = NULL;
		protected $isKnownDevice = NULL;
		protected $date = NULL;
		protected $out = NULL;
		protected $ccuConfirmedValue = NULL;
		protected $geoProwlFile = "geoProwl.php";
		protected $geoPushoverFile = "geoPushover.php";
                

                // Test if POST is send and set variables, set Date, create database objekt, create table, delete unused deviceIds, show deviceId if wanted, 
		public function __construct($config) {
                        
                    $this->getConfig($config);
                    $this->date = date('Y-m-d H:i:s');
                    $this->postData = $_POST;
                    	
                    if (isset($this->postData["device"])) {

                        $this->deviceId = $this->postData["device"];
                        $this->entry = $this->postData["entry"];
                        $this->locationName = $this->postData["name"];			
                        $this->geo_db = new PDO("sqlite:$this->dbFile");
                        $this->geo_dbCreateTable();
                        $this->geo_dbDelete();
                        $this->showId();
                        $this->verifyDeviceId($this->deviceId);
                    }
                    else {
                        $this->out = "Error: No POST data send.";
                        $this->logging();
                        exit("If logging enabled: Take a look in your logfile.");
                    }
		}
		
                private function getConfig($config) {
                    if (!empty($config)) {
                        foreach ($config as $key => $value) {
                           $this->$key = $value;
                        }
                    }
                }
                
		protected function checkSetup()  {
			
		}
		
		protected function showId() {
		
			if ($this->showDeviceId) {
				echo "Your DeviceID: " . $this->deviceId;
				exit();
			}
		}
		
		// Verify if send deviceId is known else log and exit.
		protected function verifyDeviceId($deviceId) {
		
			if (in_array($this->deviceId, $this->deviceIds)) {
				// Update Database befor set Status on CCU
				$this->geo_dbUpdate();
				// Set Status on CCU
				$this->setStatus($this->entry);
				
				if ($this->entry == 1) {
					$this->out = "Success: Entered at \"" . $this->locationName . "\" with DeviceID: " . $this->deviceId;
					}
				else {
					$this->out = "Success: Exited  at \"" . $this->locationName . "\" with DeviceID: " . $this->deviceId;
				}
				$this->logging();
			}
			else {
				$this->out = "Error: Access with unknown DeviceID: " . $this->deviceId;
				$this->logging();
				exit("If logging enabled: Take a look in your logfile.");
			}
		}
		
		// Set status on ccu
		protected function setStatus() {
			$value = $this->presentAwayLogic();
			$url = "$this->ccuUrl" . "/config/xmlapi/statechange.cgi";
			if (($this->getHttpResponseCode($url)) == 200) {
                                file_get_contents("$url" . "?ise_id=" . $this->ise_id . "&new_value=" . $value);
                                $this->ownActions();
			}
			else {
				$this->out = "Error: Destination not reachable";
				$this->logging();
                                $this->entry = 2;
				echo ("If logging enabled: Take a look in your logfile.");
			}
                        $this->pushMessages();
		}
		
		// Check if send value is 0 or 1
		protected function checkEntryValue($value) {
			if (!in_array($value, array("0", "1"))) {
				$this->out = "Error: Value not accepted.";
				$this->logging();
				exit("If logging enabled: Take a look in your logfile.");
			}
		}
		
		// Get the HTTP response code
		protected function getHttpResponseCode($theURL) {
		    $headers = get_headers($theURL);
		    return substr($headers[0], 9, 3);
		}
		
		
		// If $count more then 0 return 1 (present) else 0 (away).
		protected function presentAwayLogic() {
			$count = $this->geo_dbRead();
			if ($count > 0) {
				return 1;
			}
			else {
				return 0;
			}
		}
		
		// Generate Text for logging.
		protected function logging() {
		if ($this->showLog) {
			print_r($this->out);
		}
		$msgOut = $this->date . " - " . $this->out . "\n";
		$this->writeToLogfile($msgOut);
		}
		
		// Write log to logfile (if enabled) else delete log file.
		protected function writeToLogfile($logInformation) {
		
			if ($this->logging) {
				file_put_contents($this->logfile, $logInformation, FILE_APPEND | LOCK_EX);
				chmod($this->logfile, 0660);
			}
			else {
				if (file_exists($this->logfile)) {
					unlink($this->logfile);
				}
			}
		}
		
		// Own Actions
		
		protected function ownActions() {
			$actionUrl = $this->ccuUrl . "/config/xmlapi/";
			if ($this->entry == 1) {
				foreach ($this->ownActionsArrive as $key => $value) {
					if ($key == $this->deviceId) {
						file_get_contents($actionUrl . $value);
						// If you want to see your url in geofency enable this line:
						//echo $actionUrl . $value;
						}
					}
				}
			else {
				foreach ($this->ownActionsLeave as $key => $value) {
					if ($key == $this->deviceId) {
						file_get_contents($actionUrl . $value);
						// If you want to see your url in geofency enable this line:
						//echo $actionUrl . $value;
					}
				}
			} 
		}
		
		
		// Push Messages
		protected function pushMessages() {
			$this->prowlPush();
			$this->pushoverPush();
		}
		
		
		// Prowl Push-Notification
		protected function prowlPush() {
			if ($this->useProwl) {
				if (file_exists($this->geoProwlFile)) {
					require_once($this->geoProwlFile);
					$pushMessage = new GeoProwl($this->entry, $this->locationName);
				}
				else {
					$this->out = "Error: geoProwl.php not found.";
					$this->logging();
					exit("If logging enabled: Take a look in your logfile.");
				}
			}
		}
		
		// Pushover Push-Notification
		protected function pushoverPush() {
			if ($this->usePushover) {
				if (file_exists($this->geoPushoverFile)) {
					require_once($this->geoPushoverFile);
					$pushMessage = new GeoPushover($this->entry, $this->locationName);
				}
				else {
					$this->out = "Error: geoPushover.php not found.";
					$this->logging();
					exit("If logging enabled: Take a look in your logfile.");
				}
			}
		}
		
		/********************************************************
		* Database things										*
		********************************************************/
		
		protected function geo_dbCreateTable() {
			$this->geo_db->exec("CREATE TABLE IF NOT EXISTS devices 
			(Id INTEGER PRIMARY KEY, deviceId TEXT UNIQUE, entry INTEGER, date TEXT)");
		}
		
		// Return number of rows how many devices are presnet.
		protected function geo_dbRead() {
						
			$q = $this->geo_db->query('SELECT entry FROM devices WHERE entry = "1"');
			$rows = $q->fetchAll();
			$rowCount = count($rows);
			
			return $rowCount;						
		}
		
		// Update status of each device in db
		protected function geo_dbUpdate() {

			$this->checkEntryValue($this->entry);
			
			$sql = ('INSERT OR REPLACE INTO devices (deviceId, entry, date) 
			VALUES (:deviceId, :entry, :date)');
			
			$q = $this->geo_db->prepare($sql);
			$a = array (':deviceId'=>$this->deviceId,
		                ':entry'=>$this->entry,
		                ':date'=>$this->date);

			if ($q->execute($a)) {
		    }
		    else {
		    }                    
		}
		
		
		protected function geo_dbDelete() {
			
			$q = $this->geo_db->query('SELECT deviceId FROM devices');
			$deviceIdsFromDb = $q->fetchAll(PDO::FETCH_COLUMN, 0);
			$diff = array_diff($deviceIdsFromDb, $this->deviceIds);
						
			if (count($diff) > 0) {
				for ($c = 0; $c <= count($diff); $c++) {
					$sql = ("DELETE FROM devices WHERE deviceId = '" . $diff[$c] . "'");
					$devicesDeleted = $this->geo_db->exec($sql);
				}
			}
		}
		
		
	} // Class
	
	
	/*
         * Get config
         */
        $config = require 'config.php'; 
	
        /*
         *  Call the class GeoClass
         */
        $geo = new GeoClass($config);
?>